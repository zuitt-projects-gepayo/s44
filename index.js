// alert("hi")

// fetch() method in JS is used to request the server and load the information in the webpages.the request can be any APIs that returnsthe data of the format JSON
    
    //SYNTAX: fetch('url',options)
        //url - this is the URL to which the request is to be made
        //options - an array of properties. it is an optional

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response)=> response.json()).then((data) =>showPosts(data))



//SHOW POST
const showPosts = (posts) => {

    let postEntries = '';

    posts.forEach((post) => {

        postEntries +=  `
        
        <div id="post-${post.id}">

            <h3 id="post-title-${post.id}">${post.title}</h3>

            <p id="post-body-${post.id}">${post.body}</p>
        </div>

        <button onclick="editPost('${post.id}')" id="btn-edit-showPost">Edit</button>

        <button onclick="deletePost('${post.id}')"id="btn-delete-showPost">Delete</button>

        `;
    });
    document.querySelector("#div-post-entries").innerHTML = postEntries;
};



//DELETE POST
const deletePost = (id) => {
        
    console.log(id)

    fetch('https://jsonplaceholder.typicode.com/posts/2',
    {

        method : 'DELETE',
        headers :{ "Content-Type" : "application/json"}
    })
    .then(response => response.json())
    .then((data) => {
        console.log(data)
        alert("Post Deleted");
        document.querySelector(`#post-${id}`).remove()
        document.querySelector(`#btn-edit-showPost`).remove()
        document.querySelector(`#btn-delete-showPost`).remove()
    })
    .catch(err => console.log(err)) 

};


//Add Post
document.querySelector("#form-add-post").addEventListener("submit",(e)=>{

    e.preventDefault();

    fetch('https://jsonplaceholder.typicode.com/posts',{
        
        method : 'POST',
        body : JSON.stringify({
            title  : document.querySelector("#txt-title").value,
            body : document.querySelector("#txt-body").value,
            userId: 1
        }),
        headers :{ "Content-Type" : "application/json"}
    })
    .then((response) =>response.json())
    .then((data)=> {

        console.log(data)
        alert("Post succesfully added!")

        document.querySelector("#txt-title").value =null;
        document.querySelector("#txt-body").value =null;
    })
});


//edit post function
const editPost = (id) => {

    let title = document.querySelector(`#post-title-${id}`).innerHTML;

    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector("#txt-edit-id").value=id;

    document.querySelector("#txt-edit-title").value = title;

    document.querySelector("#txt-edit-body").value = body;

    document.querySelector("#btn-submit-update").removeAttribute('disabled')

};

//UPDATE  BUTTON
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

    e.preventDefault();

    fetch('https://jsonplaceholder.typicode.com/posts/1',{

        method : 'PUT',
        body : JSON.stringify({

            id: document.querySelector("#txt-edit-id").value,
            title  : document.querySelector("#txt-edit-title").value,
            body : document.querySelector("#txt-edit-body").value,
            userId : 1
        }),
        headers : {"Content-Type" : "application/json"}
    })

    .then((response) =>response.json())
    .then((data)=> {

        console.log(data)

        alert("Post succesfully Updated!")
        document.querySelector("#txt-edit-title").value =null;
        document.querySelector("#txt-edit-body").value =null;
        document.querySelector("#btn-submit-update").setAttribute('disabled',true);
    })

});


